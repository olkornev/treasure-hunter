

class TreasureHunter:

    def __init__(self, url=None, *args, **kwargs):
        if url:
            self.__data = self.upload_from_file(url)
        self.__treasure_path = []
        self.__coordinate = '11'
        self.searching_treasure()

    @staticmethod
    def upload_from_file(url, *args, **kwargs):
        with open(url, 'r') as test_data:
            data = [x.replace('\n', '').split(' ') for x in test_data]
            data = list(filter(lambda x: len(x) == 5, data))
        return data

    def searching_treasure(self, *args, **kwargs):
        while not self.is_treasure():
            if len(self.__treasure_path) > 25:
                self.__treasure_path = ['Treasure not found']
                break
            self.__treasure_path.append(self.__coordinate)
            first_coor = int(self.__coordinate[0])
            second_coor = int(self.__coordinate[1])
            self.__coordinate = self.__data[first_coor - 1][second_coor - 1]
        self.__treasure_path.append(self.__coordinate)

    def is_treasure(self, *args, **kwargs):
        first_coor = int(self.__coordinate[0])
        second_coor = int(self.__coordinate[1])
        return self.__data[first_coor - 1][second_coor - 1] == self.__coordinate

    @property
    def treasure(self):
        return self.__treasure_path


if __name__ == '__main__':
    path = './test_data.txt'
    TH = TreasureHunter(url=path)
    print(TH.treasure)
