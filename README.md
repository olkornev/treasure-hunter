

Both scripts for the data source use the file **test_data.txt**
The file stores numbers from 11 to 55 in the form of a two-dimensional table of 5 by 5.


To activate, use these commands in the terminal:

```bash
 $ python first_implementation.py 
```

```bash
 $ python second_implementation.py 
```

Both scripts will return the list of coordinates to the treasure in terminal, or the message ['Treasure not found'], if there are no coordinates with suitable conditions(The treasure is a cell whose value is
the same as its coordinates).

