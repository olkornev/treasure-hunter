

def searching(path_to_treasure, map_to_treasure):
    if len(path_to_treasure) > 25:
        return ['Treasure not found']
    coordinate = path_to_treasure[-1]
    first_index = int(coordinate[0])
    second_index = int(coordinate[1])
    if not map_to_treasure[first_index-1][second_index-1] == coordinate:
        new_coordinate = map_to_treasure[first_index-1][second_index-1]
        path_to_treasure.append(new_coordinate)
        return searching(path_to_treasure, map_to_treasure)
    else:
        return path_to_treasure


if __name__ == '__main__':
    path = './test_data.txt'
    with open(path, 'r') as test_data:
        data = [x.replace('\n', '').split(' ') for x in test_data]
        data = list(filter(lambda x: len(x) == 5, data))
    if data[0][0] == '11':
        print('11')
    else:
        path_to_treasure = ['11']
        answer = searching(path_to_treasure, data)
        print(answer)







